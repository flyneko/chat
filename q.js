const config = require('./config');
const App    = require('./app/App');

let app = new App({
    mongodb: process.env.MONGODB_URI || config.mongodbUri,
    port:    process.env.PORT || config.port,
    front:   true
});
app.listen();