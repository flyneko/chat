// Modules
const express         = require('express');
const http            = require('http');
const io              = require('socket.io');
const uuid            = require('uuid/v4');
const uuidValidate    = require('uuid-validate');
const sanitizeHtml    = require('sanitize-html');
const nunjucks        = require('nunjucks');
const path            = require('path');

// Database
const Database      = require('./Database');
const User          = require('./Database/models/User');
const Conversation  = require('./Database/models/Conversation');

// Models
const SocketUser      = require('./models/SocketUser');
const UserCollection  = require('./models/UserCollection');

// Components
const routes           = require('./Routes');
const controller       = require('./Controller');

module.exports = class {
    constructor(opts) {
        this.app    = express();
        this.http   = http.Server(this.app);
        this.io     = io(this.http);
        this.port   = opts.port || 80;
        this.socket = null;

        if (opts.mongodb)
            new Database({ uri: opts.mongodb });

        // Configure server
        if (opts.front == true) {
            nunjucks.configure('views', {
                autoescape: true,
                express: this.app
            });

            this.app.engine('nunj', nunjucks.render);
            this.app.set('view engine', 'nunj');

            this.app.use(express.static(path.join(__dirname, '../public')));

            // Init routes
            for (let url in routes) {
                let route = routes[url];
                this.app[route.method](url, controller[route.handler]);
            }
        }
    }

    authentication() {
        let userId = this.socket.handshake.query.userId;

        return new Promise((resolve, reject) => {
            User.findById(userId, (err, user) => {
                if (err) return;
                if (user)
                    resolve(user);
                else {
                    User.create({}, (err, user) => {
                        if (err) return;
                        resolve(user);
                    })
                }
            });
        })
    }

    _listenSocket() {
        var users = new UserCollection();

        this.io.on('connection', (socket) => {
            this.socket = socket;

            this.authentication().then((u) => {
                let user = users.add(new SocketUser(u.id, socket));
                socket.emit('auth', { id: u.id });
                //socket.emit('auth', { id: u.id, interests: u.interests_set ? u.interests : null });
                console.log('a user ' + u.id + ' connected');
                // Update online count
                this.io.sockets.emit('online users', users.getUsersCount());


                socket.on('set interests', (interests) => {
                    user.setInterests(interests);
                });

                socket.on('ban companion', (duration) => {
                    users.banConversation(user, duration);
                });

                socket.on('break conversation', (banDuration = 10) => {
                    user.getConversationId().then((id) => {
                        user.leaveConversation();
                        users.banConversation(user, banDuration);
                        socket.to(id).emit('disconnect companion');
                    });
                })

                socket.on('find companion', (interests) => {
                    user.leaveConversation().setFree().setInterests(interests);
                    users.getFreeUser(user).then((companion) => {
                        users.createConversation(user, companion).then((conversationId) => {
                            this.io.in(conversationId).emit('dialog');
                            console.log('Find companion ' + companion.getId() + ' for user ' + user.getId());
                        });
                    })
                });

                socket.on('disconnect', () => {
                    console.log('disconnect user ' + user.getId());
                    user.getConversationId().then((conversationId) => {
                        users.disconnectUser(user).then(() => {
                            socket.to(conversationId).emit('disconnect companion');
                            user = null;
                            this.io.sockets.emit('online users', users.getUsersCount());
                        });
                    });
                });

                socket.on('send message', (message) => {
                    user.getConversationId().then((conversationId) => {
                        message = sanitizeHtml(message.trim(), { allowedTags: [], allowedAttributes: []});
                        if (message == '' || !message)
                            return;
                        this.io.in(conversationId).emit('input message', { text: message, timestamp: Math.round(Date.now() / 1000), fromUser: user.getId() });
                    });
                });

                socket.on('typing', () => {
                    user.getConversationId().then((id) => socket.to(id).emit('typing'));
                });
                socket.on('stop typing', () => {
                    user.getConversationId().then((id) => socket.to(id).emit('stop typing'));
                });
            })
        });
    }

    listen() {
        this.http.listen(this.port, () => {
            console.log('listening on *:' + this.port);
            this._listenSocket();
        });
    }
}