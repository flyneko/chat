const Conversation  = require('./../Database/models/Conversation');
const User          = require('./../Database/models/User');

module.exports = class {
    constructor(userId, socket) {
        this.socket             = socket;
        this.userId             = userId;
        this.conversationId     = null;
        this.lastConversationId = null;
        this.free               = false;
        this.interests          = [];
    }

    set(vars) {
        User.findByIdAndUpdate(this.getId(), vars, (err, q) => { });
    }

    getId() {
        return this.userId;
    }

    getConversationId() {
        return new Promise((resolve, reject) => {
            resolve(this.conversationId);
        });
    }

    getLastConversationId() {
        return this.lastConversationId;
    }

    getInterests() {
        return this.interests;
    }

    isFree() {
        return this.free;
    }

    isInterestsEqual(interests) {
        return JSON.stringify(interests) == JSON.stringify(this.getInterests());
    }

    setFree(state = true) {
        this.free = state;
        return this;
    }

    setInterests(interests) {
        interests.sort();
        this.interests = interests;
        //this.set({ interests: interests, interests_set: true });
        return this;
    }

    setConversationId(id) {
        //this.set({ conversationId: id });
        this.lastConversationId = this.conversationId;
        this.conversationId = id;
        return this;
    }

    joinConversation(conversationId) {
        this.leaveConversation();
        this.setConversationId(conversationId).setFree(false);
        if (this.socket)
            this.socket.join(conversationId);
        return this;
    }

    leaveConversation() {
        this.getConversationId().then((id) => {
            if (!id) return;
            if (this.socket) this.socket.leave(id);
            this.setConversationId(null);
        });
        return this;
    }

    disconnect() {
        this.setConversationId(null);
        return this;
    }
}