const uuid = require('uuid/v4');
const User = require('./../Database/models/User');
const Conversation = require('./../Database/models/Conversation');

module.exports = class {
    constructor() {
        this._users = {};
    }

    add(user) {
        this._users[user.getId()] = user;
        return user;
    }

    banConversation(initiator, duration) {
        if (!duration) return;

        initiator.getConversationId().then((id) => {
            if (!id)
                id = initiator.getLastConversationId();
            if (!id) return;
            Conversation.findById(id, (err, conversation) => {
                if (err) return;
                conversation.setBan(duration).save();
            });
        });
    }

    createConversation(user1, user2) {
        let set = (id) => {
            user1.joinConversation(id);
            user2.joinConversation(id);
            return id;
        }

        return new Promise((resolve, reject) => {
            Conversation.findOne({ pair: { $all : [user1.getId(), user2.getId()] } }, (err, conversation) => {
                if (err) return;
                if (!conversation) {
                    Conversation.create({ pair: [user1.getId(), user2.getId()]}, (err, conv) => {
                        if (err) return;
                        resolve(set(conv.id));
                    });
                } else
                    resolve(set(conversation.id));
            });
        })
    }

    destroyConversation(id) {
        return new Promise((resolve, reject) => {
            Conversation.findById(id, (err, conversation) => {
                if (err || !conversation) return;
                conversation.pair.forEach((userId) => {
                    this.getUserById(userId).leaveConversation();
                });
                resolve();
            });
        });
    }

    disconnectUser(user) {
        let onDisconnect = () => {
            delete this._users[user.getId()];
            user.disconnect();
            user = null;
        }
        return new Promise((resolve, reject) => {
            user.getConversationId().then((id) => {
                if (!id)
                    onDisconnect();
                else
                    this.destroyConversation(id).then(() => onDisconnect());
                resolve();
            })
        });
    }

    getUsersCount() {
        return Object.keys(this._users).length;
    }

    getUserById(id) {
        return this._users[id];
    }

    getFreeUser(forUser) {
        return new Promise((resolve, reject) => {

            let freeUsers = Object.values(this._users)
                .filter((u) => u.isFree() && u.getId() != forUser.getId() && forUser.isInterestsEqual(u.getInterests()));

            freeUsers.forEach((u) => {
                Conversation.isValid(forUser.getId(), u.getId()).then((valid) => {
                    if (valid)
                        resolve(u);
                });
            });
        });
    }
}