let mongoose = require('mongoose');

module.exports = class {
    constructor(opt) {
        mongoose.connect(opt.uri);
    }
};