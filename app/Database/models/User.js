const uuid = require('uuid/v4');
const mongoose = require('mongoose');

let schema = mongoose.Schema({
    _id:            { type: String, default: () => uuid() },
    interests:      Array,
    interests_set:  { type: Boolean, default: false },
}, { _id: false })

module.exports = mongoose.model('User', schema);