const uuid = require('uuid/v4');
const mongoose = require('mongoose');

let schema = mongoose.Schema({
    _id:   { type: String, default: () => uuid() },
    pair:  Array,
    ban: {
        timestamp: Number,
        duration: Number
    }
}, { _id: false })

schema.methods.setBan = function (duration) {
    let obj = { timestamp: parseInt(Date.now() / 1000), duration: duration == -1 ? duration : duration * 60 };
    if (duration == -1) {
        this.ban = obj;
        return this;
    }
    if (this.ban.duration) {
        let banOptions = this.ban;
        if (duration >= banOptions.duration || parseInt(Date.now() / 1000) - banOptions.timestamp >= banOptions.duration)
            this.ban = obj;
    } else
        this.ban = obj;
    return this;
};

schema.statics.isValid = function (userId1, userId2) {
    return new Promise((resolve, reject) => {
        this.findOne({ pair: { $all : [userId1, userId2] } }, (err, conversation) => {
            if (err) return;
            if (!conversation || !conversation.ban.duration)
                return resolve(true);
            let banOptions = conversation.ban;
            resolve(parseInt(Date.now() / 1000) - banOptions.timestamp > banOptions.duration && banOptions.duration != -1);
        });
    });
};

module.exports = mongoose.model('Conversation', schema);