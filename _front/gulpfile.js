var gulp = require('gulp');
var webpack = require('webpack-stream');
var watch = require('gulp-watch');
var batch = require('gulp-batch');
var copy = require('gulp-copy');

// Run webpack
gulp.task('webpack', function(){
    return gulp.src('app/main.js')
        .pipe(webpack( require('./webpack.config.js') ))
        .pipe(gulp.dest('../public'))
});

// Default task
gulp.task('default', ['webpack']);