var webpack = require('webpack');
var path = require('path');

module.exports = {
    entry: [
        './app/main.js'
    ],
    output: {
        path: '/',
        filename: "app.js"
    },
    watch: true,
    module: {
        loaders: [
            {
                test: /\.js$/,
                // excluding some local linked packages.
                // for normal use cases only node_modules is needed.
                exclude: /node_modules|vue\/src|vue-router\//,
                loader: 'babel'
            },
            {
                test: /\.vue$/,
                loader: 'vue'
            }
        ]
    },
    babel: {
        presets: ['es2015'],
        plugins: ['transform-runtime']
    },
    resolve: {
        modulesDirectories: ['node_modules'],
        alias: {
            'vue$': 'vue/dist/vue.common.js',
            'semantic': path.resolve(__dirname, 'semantic/dist/semantic.min.js'),
        }
    },
    plugins: [
        new webpack.ProvidePlugin({
            // jquery
            $: 'jquery',
            moment: 'moment',
            jQuery: 'jquery',
            'window.jQuery': 'jquery',
        })
    ]
}