import Vue from 'vue';
import Resource from 'vue-resource';
import Cookie from 'vue-cookie';
import VueLocalStorage from 'vue-localstorage';
import io from 'socket.io-client';
import 'semantic';

//import store from './store'

import Message from './Message.vue';

moment.locale('ru');

// Install plugins
Vue.use(Resource);
Vue.use(Cookie);
Vue.use(VueLocalStorage, {
    name: 'ls'
});

Vue.filter('nl2br', function (str) {
    return str.replace(/(?:\r\n|\r|\n)/g, '<br />');
});

new Vue({
    ls: {
        interests: {
            type:    Array,
            default: []
        },
        userId: {
            type: String,
            default: null
        }
    },
    data: {
        // Flags
        isMounted:              false,
        isActiveDialog:         false,
        isStart:                true,
        isSearching:            false,
        isReconnecting:         false,
        isSelfTyping:           false,
        isCompanionTyping:      false,
        isDisconnectCompanion:  false,
        isBreakConversation:    false,

        // Variables
        message:    '',
        messages:   [],
        socket:     null,
        onlineCount: 0,
        interests:  [],
        banDuration: null
    },
    watch: {
        interests: function () {
            this.$ls.set('interests', this.interests);
            //this.socket.emit('set interests', this.interests);
        }
    },
    components: { Message },
    mounted: function () {
        let inputTimer = null;

        this.$nextTick(() => {
            $('.ui.checkbox').checkbox();
            $('.ui.modal').modal({ autofocus: false });
            $('.ui.dropdown').dropdown();
        });

        $(this.$refs.input).on('input', (e) => {
            clearTimeout(inputTimer);
            if (!this.isSelfTyping)
                this.socket.emit('typing');
            this.isSelfTyping = true;

            inputTimer = setTimeout(() => {
                this.isSelfTyping = false;
                this.socket.emit('stop typing');
            }, 1500);
        }).on('keyup', (e) => {
            if (e.which == 13 && !e.shiftKey)
                this.sendMessage();
        });

        this.interests = this.$ls.get('interests');

        this.connect();
    },
    methods: {
        breakConversation: function (banDuration = 0) {
            this.isBreakConversation = true;
            if (!Number.isInteger(banDuration))
                this.socket.emit('break conversation');
            else
                this.socket.emit('break conversation', banDuration);
        },
        banCompanionAndFindNext: function () {
            this.socket.emit('ban companion', 180);
            this.findCompanion();
        },
        userId: function (id) {
            if (id)
                return this.$ls.set('userId', id);
            return this.$ls.get('userId');
        },
        reset: function () {
            this.banDuration            = null;
            this.message                = '';
            this.messages               = [];
            this.isStart                = false;
            this.isSearching            = false;
            this.isReconnecting         = false;
            this.isSelfTyping           = false;
            this.isCompanionTyping      = false;
            this.isActiveDialog         = false;
            this.isDisconnectCompanion  = false;
            this.isBreakConversation    = false;
            $('.ui.modal').modal('hide');
        },
        banCompanion: function () {
            let banDurations = { '3h': 180, '24h': 1440, 'inf': -1 };
            let duration = banDurations[this.banDuration];

            if (!duration)
                return;

            if (!this.isBreakConversation)
                this.breakConversation(duration);
            else
                this.socket.emit('ban companion', duration);
            this.banDuration = true;
        },
        nextCompanion: function () {
            this.socket.emit('break conversation');
            this.$nextTick(() => this.findCompanion());
        },
        findCompanion: function () {
            this.reset();
            this.isSearching = true;
            this.socket.emit('find companion', this.$ls.get('interests'));
        },
        sendMessage: function () {
            //this.socket.emit('stop typing');
            if (this.message == '' || !this.message)
                return;
            this.socket.emit('send message', this.message);
            this.message = '';
            $(this.$refs.input).focus();
        },

        connect: function () {
            this.interestsModal('hide');
            this.socket = io(document.URL, { forceNew: true, query: { userId: this.userId() }  });
            this.socket.on('connect', () => {
                if (this.isReconnecting) {
                    this.reset();
                    this.isStart = true;
                }
            });

            // Auth user
            this.socket.on('auth', (info) =>  {
                this.isMounted = true;
                if (info.interests)
                    this.interests = info.interests;
                this.userId(info.id);
            });

            // Start dialog with user
            this.socket.on('dialog', () =>  {
                this.isSearching    = false;
                this.isActiveDialog = true;
            });

            // Input message
            this.socket.on('input message', (msg) => {
                this.messages.push(msg);
                let $container = $(this.$refs.messages);
                $container.scrollTop($container[0].scrollHeight);
            });

            // Update online count
            this.socket.on('online users', (count) => {
                this.onlineCount = count;
            });

            // Typing
            this.socket.on('typing', () => {
                this.isCompanionTyping = true;
            });
            this.socket.on('stop typing', () => {
                this.isCompanionTyping = false;
            });

            // When companion is disconnected
            this.socket.on('disconnect companion', () => {
                //this.reset();
                this.isDisconnectCompanion = true;
                //this.socket.emit('find companion');
            });

            this.socket.on('disconnect', () => {
                //this.reset();
                this.isReconnecting = true;
                $('.ui.modal').modal('hide');
            });
        },

        interestsModal: (method) => $('.js-interests-modal').modal(method),
        banModal: (method) => $('.js-ban-modal').modal(method),
        reportModal: (method) => $('.js-report-modal').modal(method)
    }
}).$mount('#app')